from service import app


@app.route('/', methods=['GET'])
def heartbeat():
    """returns a blank 200 response, for monitoring purposes"""
    return '!!'
