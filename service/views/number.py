import json
import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException
from flask import request, abort, make_response, jsonify
from voluptuous import Schema, Required, All, Length, Invalid

from service import app
from service.utils import DBSession


@app.route('/query', methods=['GET'])
def number_get():
    """
    Fetches caller id information.

    """
    schema = Schema({
        Required('number'): phone_number(),
    })
    try:
        schema(request.args.to_dict())
    except Invalid as e:
        return abort(make_response(jsonify(error=str(e)), 400))

    number = request.args.get('number')
    with DBSession() as session:
        session.execute("SELECT number, context, name FROM numbers WHERE number = '%s'" % number)
        results = session.fetchall()
    return json.dumps({'results': [dict(zip(('number', 'context', 'name'), result)) for result in results]})


@app.route('/number', methods=['POST'])
def number_post():
    """
    Adds caller id data to the service.

    """
    schema = Schema({
        Required('name'): All(str, Length(min=1)),
        Required('context'): All(str, Length(min=1)),
        Required('number'): phone_number(),
    })
    try:
        schema(request.json)
    except Invalid as e:
        return abort(make_response(jsonify(error=str(e)), 400))

    name = request.json.get('name')
    context = request.json.get('context')
    number = request.json.get('number')

    with DBSession() as session:
        rows = session.execute(
            "SELECT COUNT(*) FROM numbers WHERE number = '%s' AND context = '%s'" % (number, context)
        )
        row = rows.fetchone()
        if row[0] > 0:
            return abort(make_response(jsonify(error='number "%s" in context "%s" already exists'), 400))

        session.execute('INSERT INTO numbers (number, context, name) VALUES(?, ?, ?)', (number, context, name))

    return '', 200


def phone_number():
    def f(number):
        try:
            number = phonenumbers.parse(number, None)
        except NumberParseException:
            raise Invalid('is not in E.164 format')

        if not phonenumbers.is_valid_number(number):
            raise Invalid('invalid')
    return f
