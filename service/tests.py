import json
import unittest

EXISTING_NAME = 'Caprington Lor'
EXISTING_NUMBER = '+19849905710'
EXISTING_CONTEXT = 'party'

NAME = 'John Appleseed'
NUMBER = '+13037778899'
CONTEXT = 'apple'
CONTEXT2 = 'private'


class ServiceTest(unittest.TestCase):
    def setUp(self):
        from service import app
        self.app = app
        self.client = app.test_client()

    def post(self, url, data):
        return self.client.post(
            url,
            data=json.dumps(data),
            content_type='application/json'
        )


class HeartbeatTest(ServiceTest):
    def test_heartbeat(self):
        response = self.client.get('/')
        assert response.data == b'!!'


class NumberTest(ServiceTest):
    def test_invalid_number_get(self):
        result = self.client.get(
            '/query'
        )
        assert result.status_code == 400

        result = self.client.get(
            '/query', query_string={'number': '(984) 990-5710'}
        )
        assert result.status_code == 400

        result = self.client.get(
            '/query', query_string={'number': '+123456'}
        )
        assert result.status_code == 400

    def test_number_get(self):
        result = self.client.get(
            '/query', query_string={'number': EXISTING_NUMBER}
        )

        results = json.loads(result.data)['results']
        assert len(results) == 1

        entry = results[0]
        assert entry['number'] == EXISTING_NUMBER
        assert entry['context'] == EXISTING_CONTEXT
        assert entry['name'] == EXISTING_NAME

    def test_two_numbers_get(self):
        result = self.client.get(
            '/query', query_string={'number': '+19899100727'}
        )

        results = json.loads(result.data)['results']
        assert len(results) == 2

    def test_post(self):
        result = self.post('/number', {'name': NAME, 'number': NUMBER, 'context': CONTEXT})
        assert result.status_code == 200

        result = self.client.get(
            '/query', query_string={'number': NUMBER}
        )

        results = json.loads(result.data)['results']
        assert len(results) == 1

        entry = results[0]
        assert entry['number'] == NUMBER
        assert entry['context'] == CONTEXT
        assert entry['name'] == NAME

    def test_post_context2(self):
        result = self.post('/number', {'name': NAME, 'number': NUMBER, 'context': CONTEXT2})
        assert result.status_code == 200

        result = self.client.get(
            '/query', query_string={'number': NUMBER}
        )

        results = json.loads(result.data)['results']
        assert len(results) == 2

        entry = results[1]
        assert entry['number'] == NUMBER
        assert entry['context'] == CONTEXT2
        assert entry['name'] == NAME

    def test_post_duplicate(self):
        result = self.post('/number', {'name': NAME, 'number': NUMBER, 'context': CONTEXT})
        assert result.status_code == 400


if __name__ == '__main__':
    unittest.main()
