from flask import Flask

app = Flask(__name__)
app.config.from_object('config_local.Config')

from service.utils import import_seed_data
import_seed_data()

from service import views
