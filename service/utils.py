import csv
import sqlite3

import os
import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException

from service import app


class DBSession(object):
    DB_PATH = os.path.join(app.root_path, '../db/db.sqlite3')

    def __enter__(self):
        self.cnx = sqlite3.connect(self.DB_PATH)
        self.cursor = self.cnx.cursor()
        return self.cursor

    def __exit__(self, type, value, traceback):
        self.cnx.commit()
        self.cnx.close()


def import_seed_data():
    """
    Populate in-memory SQLite3 database with seed data

    """
    seed_file = os.path.join(app.root_path, '../data/interview-callerid-data.csv')

    entries = []
    with open(seed_file, 'r') as f:
        rows = csv.DictReader(f)

        for row in rows:
            # First try to parse a standard E.164 number
            # If that doesn't work, assume international code +1
            # If that doesn't work, skip this number as invalid
            try:
                number = phonenumbers.parse(row['number'], None)
            except NumberParseException:
                try:
                    number = phonenumbers.parse(row['number'], 'US')
                except NumberParseException:
                    continue

            # Normalize all numbers
            number = phonenumbers.format_number(number, phonenumbers.PhoneNumberFormat.E164)
            entries.append((number, row['context'], row['name']))

    # Unique entries only
    entries = list(set(entries))

    print('Imported %s entries from "%s"' % (len(entries), seed_file))

    with DBSession() as session:
        session.execute('DROP TABLE IF EXISTS numbers')
        session.execute('CREATE TABLE numbers (number, context, name)')
        session.executemany('INSERT INTO numbers (number, context, name) VALUES (?, ?, ?)', entries)
