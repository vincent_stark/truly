# Truly.co Test Project

The goal of this exercise is for you to implement a standalone service that will respond to requests seeking caller id information.

## API Requirements/Notes

API should return json.
Phone numbers should be in E.164 format.
Appropriate http codes should be returned on error.

# Running locally

* Install dependencies and run app
```bash
pip install -r requirements.txt
python application.py
```

* App imports the seed CSV (500k records) on each run. It takes around 30 seconds on a powerful machine.

* After import is complete, the app becomes available at: http://0.0.0.0:5000/
